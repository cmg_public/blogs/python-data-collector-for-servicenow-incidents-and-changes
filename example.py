import requests
import csv

# ServiceNow credentials and URLs
instance_url = "YOUR_INSTANCE"
username = "YOUR_USERNAME"
password = "YOUR_PASSWORD"
incident_endpoint = "/api/now/table/incident"
change_endpoint = "/api/now/table/change_request"

# Set up the request headers
headers = {"Content-Type": "application/json", "Accept": "application/json"}

# Set your date range for filtering
start_date = "2018-01-01 00:00:00"
end_date = "2023-01-31 23:59:59"

# Construct the query to filter by date range
date_query = f'sys_created_on>=javascript:gs.dateGenerate("{start_date}")^sys_created_on<=javascript:gs.dateGenerate("{end_date}")'

params = {"sysparm_query": date_query}


# Function to fetch data from ServiceNow
def fetch_data(endpoint):
    response = requests.get(
        endpoint, auth=(username, password), headers=headers, params=params
    )
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to fetch data. Status code: {response.status_code}")
        return None

# Function to write data to CSV
def write_to_csv(data, filename):
    with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames=data[0].keys())
        csv_writer.writeheader()
        csv_writer.writerows(data)

# Fetch Incidents data
incidents_data = fetch_data(instance_url + incident_endpoint)
if incidents_data:
    # Process or store incidents_data as needed
    write_to_csv(incidents_data["result"], 'incidents_data.csv')

# Fetch Changes data
changes_data = fetch_data(instance_url + change_endpoint)
if changes_data:
    # Process or store changes_data as needed
    write_to_csv(changes_data["result"], 'changes_data.csv')
